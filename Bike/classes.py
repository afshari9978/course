class Bike:
    def __init__(self, name):
        self.name = name
        self.need_repair = False

    def __str__(self):
        return self.name


class BikeDepot:
    garage_bikes = []
    store_bikes = []

    def __init__(self, bikes_count: int = 0):
        for i in range(bikes_count):
            self.store_bikes.append(Bike(f'{i}'))

    def move_from_garage_to_store(self):
        pass

    def move_from_store_to_garage(self):
        pass

    def load(self):
        file = open('data.txt')
        for line in file.readlines():
            self.store_bikes.append(Bike(name=line))

        print(len(self.store_bikes))


bd = BikeDepot()
bd.load()

a = 1
