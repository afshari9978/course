from datetime import datetime
from typing import List


class RealBook:
    def __init__(self, book, id, entrance_year):
        self.book = book
        self.id = id
        self.entrance_year = entrance_year
        self.borrow_list = []
        self.reserve_list = []
        self.available = True

    def is_available(self) -> bool:
        return self.available

    def borrow(self, student, end_date):
        if not self.is_available():
            return 'Not Available'
        self.available = False
        borrow = Borrow(student, "1399", end_date, self)
        self.borrow_list.append(borrow)


class Author:
    def __init__(self, title: str):
        self.title = title


class Book(object):
    library_name = 'FUMLIB'

    def __init__(
            self, title: str, pages: int,
            publication_year, authors: List['Author'],
            location
    ):
        self.title: str = title
        self.pages = pages
        self.publication_year = publication_year
        self.authors = authors
        self.location = location

    def set_location(self, location):
        self.location = location


class Student:
    def __init__(self, student_number, valid_until_year):
        self.student_number = student_number
        self.valid_until_year = valid_until_year


class Borrow:
    def __init__(self, student: Student, start_date, end_date, real_book):
        self.student = student
        self.start_date = start_date
        self.end_date = end_date
        self.cost = 0
        self.real_book = real_book

    def add_amount_to_cost(self, amount):
        if amount >= 0:
            self.cost += amount
        else:
            if self.cost >= abs(amount):
                self.cost += amount
            else:
                self.cost = 0

    def get_cost(self, now: int):
        if now > self.end_date:
            penalty_amount = (now - self.end_date) * 100
            return self.cost + penalty_amount
        return self.cost


class Reserve:
    def __init__(self, book, student):
        self.book = book
        self.student = student

    def can_borrow(self, real_books: List[RealBook]):
        for real_book in real_books:
            if real_book.book == self.book:
                if real_book.is_available():
                    return True
        return False
