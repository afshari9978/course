class Player:
    players = []

    def __init__(self, name, is_white: bool):
        if len(self.players) == 2:
            raise Exception('max players reached')
        self.name = name
        self.set_is_white(is_white)
        self.players.append(self)

    def set_is_white(self, is_white):
        if len(self.players) > 0:
            self.is_white = not self.players[0].is_white
        else:
            self.is_white = is_white


class Chessman:

    chessmen = []

    def __init__(self, x, y, is_white):
        self.x = x
        self.y = y
        self.is_white = is_white
        self.fixed_moves = self.add_fixed_moves()

        self.check()

        self.chessmen.append(self)

    def check(self):
        raise NotImplementedError()

    def add_fixed_moves(self):
        raise NotImplementedError()

    def get_available_moves(self):
        raise NotImplementedError()


class Pawn(Chessman):

    def check(self):
        count = 0
        for chessman in self.chessmen:
            if chessman.is_white == self.is_white:
                count += 1
        if count == 8:
            raise Exception('maximum pawns allowed is 8')


    def add_fixed_moves(self):
        all = []

        if self.is_white:
            all.append((0, 1))

        return all


class Board:
    ROWS = 8
    COLS = 8

    in_game_chessmen = []
    out_chessmen = []


    def __init__(self, player1: Player, player2: Player):
        self.player1 = player1
        self.player2 = player2
        self.create_chessmens()

    def create_chessmens(self):
        raise NotImplementedError('create_chessmen')


if __name__ == '__main__':
    name1 = input("enter your name: ")
    is_white1 = bool(input("is your color white[True/False]: "))
    player1 = Player(name1, is_white1)

    name2 = input("enter your name: ")
    is_white2 = bool(input("is your color white[True/False]: "))
    player2 = Player(name2, is_white2)
